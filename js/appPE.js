function ajax(){
    const url = "https://jsonplaceholder.typicode.com/users";

    axios
    .get(url)
    .then((res)=>{
        mostrarDatos(res.data);

    })
    .catch((error)=>{
        console.log("Surgió un error" + error);
    })
    function mostrarDatos(data){

        var idd = document.getElementById("idd").value;
        var limp = document.getElementById("idd");
        var nombre = document.getElementById("nombre");
        var nombreUsu = document.getElementById("nombreUsu");
        var email = document.getElementById("email");
        var calle = document.getElementById("calle");
        var numero = document.getElementById("numero");
        var ciudad = document.getElementById("ciudad");

            for(item of data){
                if(idd == item.id){
                    nombre.value = item.name
                    nombreUsu.value = item.username
                    email.value = item.email
                    calle.value = item.address.street
                    numero.value = item.address.suite
                    ciudad.value = item.address.city
                }else{
                    console.log("Ocurrió un error!");
                }

            }
        
            document.getElementById('btnLimpiar').addEventListener('click', function(){
                idd.value=""
                limp.value=""
                nombre.value=""
                nombreUsu.value=""
                email.value = ""
                calle.value = ""
                numero.value = ""
                ciudad.value = ""
            })
    }// cierra función mostrar
    
} // cierra función ajax
const btnBuscar = document.getElementById('btnBuscar');
btnBuscar.addEventListener('click', function(){
    ajax();
})